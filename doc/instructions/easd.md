
# Poster

- Only pdf
- A0 format, landscape
- At least 200 dpi
- Include brief explanation of the Aims, Methods, Results and Conclusions,
as well as disclosure information.

# Audio

- 3-4 minute presentation
- MP3 format
