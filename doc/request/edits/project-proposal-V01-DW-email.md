
Here are my comments and edits. I think we will have to provide a bit more
detail regarding the exact biomarkers we are requesting. We will also need to
specify the subpopulations we are intending to analyse: in all likelihood 2: one
limited to participants with available MRI and DEXA data, and one limited only
by availability of biomarkers + determinants + outcomes. We will also need to
say a few words regarding the extraction of info on liver fat from MRI images..
is that work that has already been done by somebody or will we have to do it as
part of the project?

We currently don’t mention any genetics data or analyses..? I don’t remember
whether that’s what we decided when we discussed things via Skype? Or did we say
we wanted to include polygenic risk scores?
